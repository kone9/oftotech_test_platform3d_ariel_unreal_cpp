# OFTOSOFT TEST PLATFORM 3D C++ en Unreal Engine por Ariel Gimenez

## Un ejemplo con mecánicas de juego plataformas desarrollado en unreal engine con C++, donde hay que destruir a todos los enemigos, agarrar el kit upgrade y terminar el juego en la plataforma más alta del juego.

## El juego fue desarrollado con el pensamiento de reutilizar los assets, por eso se crearon componentes c++ que van dentro de los diferentes actores. Tambien se agregaron mécanicas al ThirdPersonCPP principal, un gamemode, un game instance, level sequencer para manejar los enemigos, entre otros

## Modelado e Blender3D, Alguans animacioens fueron hechas en Blender3D usando el plugin UE to Rigiffy, tambien importe modelos de otros lados como el robot que forma parte del platform3D del motor Godot Engine

## ichio download game: https://kone9.itch.io/platform-unreal-engine-example

## If you don't know how to download the repository, since you need an SSH key on your PC, check out this tutorial: https://www.youtube.com/watch?v=Vmt0V6a3ppE 

# Teclado, W,S,A,D,barra espacio,q ,click izquierdo, 

# joystick palancas, x,cuadrado,circulo, triangulo, L1

# ejemplo_estatico
![GamePlay](ejemplo_estatico.gif)

# ejemplo_menu
![GamePlay](ejemplo_menu.gif)

# ejemplo_golpe
![GamePlay](ejemplo_golpe.gif)

# ejemplo_ataque_distancia_con_congelada
![GamePlay](ejemplo_ataque_distancia_con_congelada.gif)

# ejemplo_ataque_enemigo_con_rayo_muerte_jugador_reiniciar
![GamePlay](ejemplo_ataque_enemigo_con_rayo_muerte_jugador_reiniciar.gif)

# ejemplo_kit_life
![GamePlay](ejemplo_kit_life.gif)

# ejemplo_salto_simple_agachar
![GamePlay](ejemplo_salto_simple_agachar.gif)

# ejemplo_matar_enemigo
![GamePlay](ejemplo_matar_enemigo.gif)

# ejemplo_obstaculo_saca_energia
![GamePlay](ejemplo_obstaculo_saca_energia.gif)

# ejemplo_kit_upgrade_doble_jump
![GamePlay](ejemplo_kit_upgrade_doble_jump.gif)


# ejemplo_guardar_arma
![GamePlay](ejemplo_guardar_arma.gif)


## --------------------------------------------- 
## Algunas mecánicas.

combo 2 ataques.

doble jump

kit life.

kit upgrade

barra de vida.

muerte jugador

muerte enemigo.

diseño de nivel tipo platform.

Trampa saca vida mientras estás arriba

efectos visuales al ser destruido enemigo.

efectos visuales al agarrar kit upgrade

efectos visuales al agarrar kit life

si subes arriba de todo podes agarrar el  kit upgrade para activar el doble salto.

Gamehandler respawn muerte,

respawn con delay 

Gamehandler dificultad.

nivel para seleccionar dificultad con pantalla de inicio

personaje super golpe con combo 3

personaje arroja magia con hielo

enemigo lanza efecto rayo cuando golpea al jugador

contador para saber cuando todos los enemigos están muertos.

musica ambiente

efectos de sonido

ganar el juego.

## --------------------------------------------------------
## A tener en cuenta sobre el proyecto

-En el proyecto pedian un golpe fuerte pero no lo hice, aunque el proyecto tiene el montage preparado para eso.

-Pedian reiniciar el personaje y iniciar paulatinamente, en ese caso cree la animación muerte con un tiempo hasta reiniciar, cuando reinicia el juego comienza de golpe.

-Pueden Fallar las UPROPERTIES con las referencias: Si es el caso de que no agarra un item o no hace alguna animación es porque falta las referencias.. Por alguna razón tuve varios bugs con esto, la solución fue sacar el componente actor C++ y volverlo a colocar en el blueprint, luego puse las referencias que necesitan los componentes... Puede que falle esto, ya que hay un gitignore, espero que no.

-El proyecto tiene partes de ingles y partes en español.

-Hay algunos problemas con las texturas y el UV map del mapa.