// Copyright Epic Games, Inc. All Rights Reserved.

#include "Octosoft_Test_ArielCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"


#include <Kismet/GameplayStatics.h>
#include "Games_rules_game_Mode.h"
#include "Components/BoxComponent.h"
//#include "Components/AudioComponent.h"


#include <Engine/World.h>
//#include "Kismet/KismetMathLibrary.h"


//#include "Components/ArrowComponent.h"





//////////////////////////////////////////////////////////////////////////
// AOctosoft_Test_ArielCharacter

AOctosoft_Test_ArielCharacter::AOctosoft_Test_ArielCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	///audio
	//jump_sound = CreateDefaultSubobject<UAudioComponent>(TEXT("Jump_sound"));
	//CameraBoom->SetupAttachment(RootComponent);


	montaje_double_jump = nullptr;
	Attack_montage = nullptr;

	impulse_double_jump = 25000;

	game_mode = nullptr;

	can_chain_attack = false;

	blueprintAnimation = nullptr;

	character_component = nullptr;

	attack_count = 1;

	active_weapon = false;
}


void AOctosoft_Test_ArielCharacter::BeginPlay()
{
	Super::BeginPlay();

	game_mode = UGameplayStatics::GetGameMode(GetWorld());
	blueprintAnimation = GetMesh()->GetAnimInstance();
	character_component = GetCharacterMovement();
	
	sword_in_character = attach_ice_effect(sword, "Hand_RSocket");

	attack_count = 1;
	
	if (sword_in_character == nullptr) return;
	sword_collision_comp = Cast< UBoxComponent> (sword_in_character->GetComponentByClass(UBoxComponent::StaticClass() ) ); //example of how to find a component using the reference class



//////////////

}

//cuando este personaje va a ser destruido
void AOctosoft_Test_ArielCharacter::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	//
	if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, TEXT("Destruyeron el personaje, enviando mensaje desde el END PLAY"));
}

//.cpp
void AOctosoft_Test_ArielCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	/*const bool my_test_bool = can_chain_attack;
	if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 0, FColor::Yellow, FString::Printf(TEXT("Bool: %s"), my_test_bool ? TEXT("true") : TEXT("false")));*/
	
	//int int_to_print = attack_count;
	//if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 0, FColor::Yellow, FString::Printf(TEXT("int attack_count: %i"), int_to_print));



}

//////////////////////////////////////////////////////////////////////////
// Input

void AOctosoft_Test_ArielCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction( "Jump", IE_Pressed, this, &AOctosoft_Test_ArielCharacter::Jump_and_dobleJump );//jump ariel Octosoft
	PlayerInputComponent->BindAction( "attack_input", IE_Pressed, this, &AOctosoft_Test_ArielCharacter::Attack_Octosoft_ariel);//attack ariel Octosoft
	PlayerInputComponent->BindAction( "input_active_desactivee_weapon", IE_Pressed, this, &AOctosoft_Test_ArielCharacter::active_desactivee_weapon);//attack ariel Octosoft
	PlayerInputComponent->BindAction( "input_magic_attack", IE_Pressed, this, &AOctosoft_Test_ArielCharacter::magic_attack);//attack ariel Octosoft

	//crounch
	PlayerInputComponent->BindAction("Crounch", IE_Pressed, this, &AOctosoft_Test_ArielCharacter::cruch_input);
	PlayerInputComponent->BindAction("Crounch", IE_Released, this, &AOctosoft_Test_ArielCharacter::uncrach_input);

	PlayerInputComponent->BindAxis("MoveForward", this, &AOctosoft_Test_ArielCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AOctosoft_Test_ArielCharacter::MoveRight);


	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AOctosoft_Test_ArielCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AOctosoft_Test_ArielCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &AOctosoft_Test_ArielCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &AOctosoft_Test_ArielCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AOctosoft_Test_ArielCharacter::OnResetVR);
}


void AOctosoft_Test_ArielCharacter::Jump_and_dobleJump()//jump ariel
{
	if (Attack_montage == nullptr) return;//si hay un puntero
	if (hide_weapon_montage == nullptr) return;//si hay un puntero
	if (show_weapon_montage == nullptr) return;//si hay un puntero
	if (character_component == nullptr) return;//si hay un puntero
	if (game_mode == nullptr) return;//si hay un puntero
	if (montaje_double_jump == nullptr) return;//si hay un puntero


	if (blueprintAnimation->Montage_IsPlaying(Attack_montage) == true) StopAnimMontage(Attack_montage);//stop montaje anterior 
	

	ACharacter::Jump();

	if (Cast< AGames_rules_game_Mode>(game_mode)->super_double_jump)//si tengo activado doble jump
	{
		if (JumpMaxCount == 1)//habilita doble salto
		{
			JumpMaxCount = 2;
		}

		if (JumpCurrentCount == 1)
		{
			//si hago super salto no puedo sacar o poner arma
			if (blueprintAnimation->Montage_IsPlaying(hide_weapon_montage) == true) StopAnimMontage(hide_weapon_montage);//stop montaje anterior 
			if (blueprintAnimation->Montage_IsPlaying(show_weapon_montage) == true) StopAnimMontage(show_weapon_montage);//stop montaje anterior 
			
			//agrego impulso y pongo animacion super salto
			character_component->AddImpulse(FVector::UpVector * impulse_double_jump);//impulse in doble jump
			PlayAnimMontage(montaje_double_jump);
		
		}
	}



}

void AOctosoft_Test_ArielCharacter::active_desactivee_weapon()
{
	if (Attack_montage == nullptr) return;//si no hay un puntero
	if (show_weapon_montage == nullptr) return;//si no hay un puntero
	if (hide_weapon_montage == nullptr) return;//si no hay un puntero
	if (sword_in_character == nullptr) return;//si no hay un puntero
	if (blueprintAnimation == nullptr) return;//si no hay un puntero
	if (character_component->IsFalling() == true && JumpCurrentCount == 2) return;// Si esta en el aires y hace super salto
	
	if (blueprintAnimation->Montage_IsPlaying(Attack_montage) == true)
	{
		StopAnimMontage(Attack_montage);//stop montaje anterior 
	}
	
	if (active_weapon == true)
	{
		PlayAnimMontage(hide_weapon_montage);//inicio montage playa animacion
		active_weapon = false;
		//FName socket_hand = TEXT("Hand_RSocket");
		//sword_in_character->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, socket_hand);
	}
	else
	{
		PlayAnimMontage(show_weapon_montage);//inicio montage playa animacion
		active_weapon = true;
		//FName socket_back = TEXT("espalda_socket");
		//sword_in_character->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, socket_back);
	}
}

void AOctosoft_Test_ArielCharacter::magic_attack()
{
	if (magic_attack_montage == nullptr) return;//si el puntero es null return
	if (hide_weapon_montage == nullptr) return;//si el puntero es null return
	if (show_weapon_montage == nullptr) return;//si el puntero es null return
	if (blueprintAnimation->Montage_IsPlaying(magic_attack_montage) == true) return;
	if (blueprintAnimation->Montage_IsPlaying(hide_weapon_montage) == true) return;
	if (blueprintAnimation->Montage_IsPlaying(show_weapon_montage) == true) return;
	if (character_component->IsFalling() == true) return;// Si estoy en el aire
	if (character_component->IsCrouching() == true) return;// Si estoy agachado

	PlayAnimMontage(magic_attack_montage);

}

void AOctosoft_Test_ArielCharacter::Attack_Octosoft_ariel()
{
	//if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, TEXT("Tendria que golpear"));
	
	if (Attack_montage == nullptr) return;//si hay un montage
	if (character_component == nullptr) return;//si hay un character
	if (character_component->IsFalling() == true) return;// Sino esta en el aire
	if (character_component->IsCrouching() == true) return;// Sino estoy agachado
	if (active_weapon == true) return;// Solo si tengo el arma en la mano puedo golpear
	if (blueprintAnimation->Montage_IsPlaying(hide_weapon_montage) == true) return;
	if (blueprintAnimation->Montage_IsPlaying(show_weapon_montage) == true) return;

	if (blueprintAnimation->Montage_IsPlaying(Attack_montage) == false)//si no se ejecuta el montage lo inicio
	{
		PlayAnimMontage(Attack_montage);
	}
	else//si esta iniciado el montage
	{
		if (can_chain_attack == true)//paso a la siguiente seccion del montage si presiono boton en momento justo, esto lo compruebo en el anim blueprint con la variable publica can_chain_attack
		{
			/*attack_count += 1;
			FString montage_seccion;
			switch (attack_count)
			{
			case 1:
				montage_seccion = "attack_2";
				break;

			case 2:
				montage_seccion = "attack_3";
				break;
			}*/

			//FName new_Section_fname = FName(*montage_seccion);//new section
			FName new_Section_fname = TEXT("attack_2");//new section
			//cambio a la siguiente seccion del montage
			blueprintAnimation->Montage_SetNextSection(
					blueprintAnimation->Montage_GetCurrentSection(Attack_montage),
				new_Section_fname,
					Attack_montage
				);
			
			can_chain_attack = false;//it's false until i can press again in the next section
			
			//if (attack_count == 3) attack_count = 1;
		}
	}
	

	
}

void AOctosoft_Test_ArielCharacter::cruch_input()
{
	if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, TEXT("TENDRIA QUE AGACHARSE"));

	if (character_component->IsFalling() == true) return;// Sino esta en el piso
	Crouch();
	character_component->MaxWalkSpeed = 100;
}

void AOctosoft_Test_ArielCharacter::uncrach_input()
{
	if (character_component->IsFalling() == true) return;// Sino esta en el piso

	UnCrouch();
	character_component->MaxWalkSpeed = 600;
	if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, TEXT("TENDRIA QUE PARARSE"));
}

//SPAWN actor to socket name
AActor* AOctosoft_Test_ArielCharacter::attach_ice_effect(TSubclassOf<AActor> gunClass, FName socketName)
{
	const FTransform orientation_socket = GetMesh()->GetSocketTransform(socketName, ERelativeTransformSpace::RTS_World);

	AActor* newGun = GetWorld()->SpawnActor(gunClass, &orientation_socket);//FTransform por referencia constante
	if (newGun == nullptr) return nullptr;

	newGun->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, socketName);
	
	return newGun;
}

void AOctosoft_Test_ArielCharacter::OnResetVR()
{
	// If Octosoft_Test_Ariel is added to a project via 'Add Feature' in the Unreal Editor the dependency on HeadMountedDisplay in Octosoft_Test_Ariel.Build.cs is not automatically propagated
	// and a linker error will result.
	// You will need to either:
	//		Add "HeadMountedDisplay" to [YourProject].Build.cs PublicDependencyModuleNames in order to build successfully (appropriate if supporting VR).
	// or:
	//		Comment or delete the call to ResetOrientationAndPosition below (appropriate if not supporting VR)
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AOctosoft_Test_ArielCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
		Jump();
}


void AOctosoft_Test_ArielCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
		StopJumping();
}



void AOctosoft_Test_ArielCharacter::sword_show()//lo llamo desde el anim blueprint con un evento desde el anim montage
{
	if (sword_in_character == nullptr) return;//si hay un montage
	FName socket_hand = TEXT("Hand_RSocket");
	sword_in_character->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, socket_hand);
	active_weapon = false;
	if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, TEXT("arma COLOCADA en la mano"));
}

AActor* AOctosoft_Test_ArielCharacter::spawn_proyectil_to_forward_vector_in_socket_position(TSubclassOf<AActor> proyectil_class, FName socketName)
{
	///////////////
	//.CPP
	//#include <Engine/World.h> // Don't forget to include World.h to use methods inside UWorld Class
	if (GetWorld() == nullptr) return nullptr;
	if (proyectil_class == nullptr) return nullptr;

	FActorSpawnParameters spawn_params{};
	spawn_params.Owner = nullptr; // Specify owner actor for this new actor if you need one.
	spawn_params.Instigator = nullptr; // Specify instigator pawn for this new actor if you need one
	spawn_params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	FTransform new_actor_transform = FTransform::Identity; // Identity Matrix if you need the default transformation.
	FVector socket_position = GetMesh()->GetSocketTransform(socketName, ERelativeTransformSpace::RTS_World).GetLocation();

	new_actor_transform.SetLocation(socket_position);
	new_actor_transform.SetRotation(GetActorRotation().Quaternion());

	// Spawn Actor return a Pointer to new Actor
	AActor* newProyectil = GetWorld()->SpawnActor<AActor>(proyectil_class, new_actor_transform, spawn_params);
	
	return newProyectil;
}

//esto es llamado desde el animblueprint
void AOctosoft_Test_ArielCharacter::spawn_proyectil()
{
	if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, TEXT("instance magic"));
	
	spawn_proyectil_to_forward_vector_in_socket_position(magic_proyectil, "Hand_RSocket_magic");

}



void AOctosoft_Test_ArielCharacter::sword_hide()//lo llamo desde el anim blueprint con un evento desde el anim montage
{
	if (sword_in_character == nullptr) return;//si hay un montage
	FName socket_back = TEXT("espalda_socket");
	sword_in_character->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, socket_back);
	active_weapon = true;
	if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, TEXT("arma en la espalda"));
}

void AOctosoft_Test_ArielCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AOctosoft_Test_ArielCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AOctosoft_Test_ArielCharacter::MoveForward(float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AOctosoft_Test_ArielCharacter::MoveRight(float Value)
{
	if ( (Controller != nullptr) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}













