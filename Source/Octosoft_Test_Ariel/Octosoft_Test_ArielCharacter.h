// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Octosoft_Test_ArielCharacter.generated.h"

UCLASS(config=Game)
class AOctosoft_Test_ArielCharacter : public ACharacter
{
	GENERATED_BODY()


protected:
	virtual void BeginPlay() override;

	/** Overridable function called whenever this actor is being removed from a level */
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
public:
	AOctosoft_Test_ArielCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	//
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class UAnimMontage* montaje_double_jump;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class UAnimMontage* Attack_montage;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class UAnimMontage* show_weapon_montage {nullptr};
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class UAnimMontage* hide_weapon_montage {nullptr};
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class UAnimMontage* magic_attack_montage {nullptr};

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float impulse_double_jump;

	UPROPERTY(EditAnywhere)
	TSubclassOf<AActor> sword{};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<AActor> magic_proyectil{};

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	bool can_chain_attack;//puedo encandenar ataques?

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class AActor* sword_in_character;
	

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class UBoxComponent* sword_collision_comp;

	UAnimInstance* blueprintAnimation;//get anim instance
	
	UCharacterMovementComponent* character_component;//get character movement
	
	//UPROPERTY(EditAnywhere, BlueprintReadWrite)
	//class UArrowComponent* magic_position_spawn {nullptr};

private:
	int attack_count;

	bool active_weapon;


private:
	class AGameModeBase* game_mode;

private:
	AActor* attach_ice_effect(TSubclassOf<AActor> gunClass, FName socketName);
	


protected:
	AActor* spawn_proyectil_to_forward_vector_in_socket_position(TSubclassOf<AActor> proyectil_class, FName socketName);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	/** Active and desactive weapon se llama desde el anim blueprint*/
	UFUNCTION(BlueprintCallable)
	void sword_show();

	//** Active and desactive weapon se llama desde el anim blueprint*/
	UFUNCTION(BlueprintCallable)
	void sword_hide();

	//** Active and desactive weapon se llama desde el anim blueprint*/
	UFUNCTION(BlueprintCallable)
	void spawn_proyectil();


	void active_desactivee_weapon();//funciona con el evento del teclado

	/** Jump_and_doble_jump */
	void Jump_and_dobleJump();

	/** Attack with montage sword*/
	void Attack_Octosoft_ariel();

	/** Attack with montage magic*/
	void magic_attack();

	/** Crounch */
	void cruch_input();
	void uncrach_input();

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }


//public:
//	UPROPERTY(EditAnywhere, BlueprintReadWrite)
//	class UAudioComponent* jump_sound{ nullptr };



};

