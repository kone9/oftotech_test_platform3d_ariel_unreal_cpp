// Fill out your copyright notice in the Description page of Project Settings.


#include "obstacle_event/Obstacle_BoxTrigger_damage.h"
#include "DrawDebugHelpers.h"
#include <Engine/World.h>



AObstacle_BoxTrigger_damage::AObstacle_BoxTrigger_damage()
{
	drawDebug = false;

}



void AObstacle_BoxTrigger_damage::BeginPlay()
{
	Super::BeginPlay();
	
	OnActorBeginOverlap.AddDynamic(this, &AObstacle_BoxTrigger_damage::ActorBeginOverlap);

	if (drawDebug)
	{
		DrawDebugBox(GetWorld(), GetActorLocation(), GetComponentsBoundingBox().GetExtent(), FColor::Purple, true, 999, 0, 5);
	}

}


void AObstacle_BoxTrigger_damage::ActorBeginOverlap(class AActor* MyOverlappedActor, class AActor* OtherActor)
{
	/*if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, TEXT("hay que descontarle vida al jugador"));
	if (OtherActor != nullptr)
	{
		if (OtherActor->ActorHasTag("Jugador"))
		{
		}
	}*/

}

