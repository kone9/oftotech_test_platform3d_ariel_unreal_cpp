// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy/Enemy.h"
#include "Components/BoxComponent.h"
#include <Engine/World.h>
#include <GameFramework/Character.h>

#include <LevelSequenceActor.h>
#include <LevelSequencePlayer.h>
#include <MovieSceneSequencePlayer.h>
#include <Engine/World.h>

#include <Components/WidgetComponent.h>
#include <Components/CapsuleComponent.h>

#include <Particles/ParticleSystem.h>
#include "Particles/ParticleSystemComponent.h"


#include <Kismet/GameplayStatics.h>

#include "Games_rules_game_Mode.h"

#include "Components/AudioComponent.h"

// Sets default values
AEnemy::AEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Box_Damage_player = CreateDefaultSubobject<UBoxComponent>(TEXT("Box_Damage_player"));
	Box_Damage_player->AttachTo(RootComponent);

	Box_Damage_Enemy = CreateDefaultSubobject<UBoxComponent>(TEXT("Box_Damage_Enemy"));
	Box_Damage_Enemy->AttachTo(RootComponent);

	attack_sound = CreateDefaultSubobject<UAudioComponent>(TEXT("Attack_sound"));
	attack_sound->AttachTo(RootComponent);

	frozen_sound = CreateDefaultSubobject<UAudioComponent>(TEXT("Frozen_sound"));
	frozen_sound->AttachTo(RootComponent);

	life_Enemy = 100;
	blueprintAnimation = nullptr;
	can_receive_hit = true;
	
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();
	
	if (Box_Damage_player == nullptr) return;
	if (Box_Damage_Enemy == nullptr) return;
	if (GetWorld() == nullptr) return;
 	
	Box_Damage_player->OnComponentBeginOverlap.AddDynamic(this, &AEnemy::on_component_begin_overlap_damage_Player);
	Box_Damage_Enemy->OnComponentBeginOverlap.AddDynamic(this, &AEnemy::on_component_begin_overlap_damage_enemy);

	blueprintAnimation = GetMesh()->GetAnimInstance();
	
	game_mode = UGameplayStatics::GetGameMode(GetWorld());

	//Por alguna raz�n no funciona buscar este componente desde C++, lo busco directo desde el blueprint con un puntero BlueprintReadWrite
	//AActor* owner = GetOwner();
	//if (owner == nullptr) return;
	//TSubclassOf< UWidgetComponent> widget;
	//Component_widget = Cast< UWidgetComponent>(owner->GetComponentByClass( widget )); //example of how to find a component using the reference class
	//omponent_widget = Cast< UWidgetComponent>( owner->GetComponentsByTag(UWidgetComponent::StaticClass(), TEXT("life_barra_widget"))[0] ); //example of how to find a component using the reference class

}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if (instance_particle == true)
	{
		if (new_particle_effect == nullptr) return;
		if (ref_character == nullptr) return;
		FVector postion_character = ref_character->GetMesh()->GetSocketLocation(TEXT("spine_03_Socket"));
		new_particle_effect->SetBeamTargetPoint(0, postion_character, 0);
	}

}

// Called to bind functionality to input
void AEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

//PLAYER EFFECT DAMAGE
void AEnemy::on_component_begin_overlap_damage_Player(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor == nullptr) return;
	if (GetWorld() == nullptr) return;
	if (attack_sound == nullptr) return;


	if (OtherActor->ActorHasTag("Jugador"))
	{
		if (can_attack)
		{
			//UGameplayStatics::SpawnEmitterAttached(particle_effect_damage_player, GetRootComponent(), NAME_None, GetActorLocation(), GetActorRotation(), EAttachLocation::SnapToTarget, false);

			if (instance_particle == false)
			{
				//instance particles
				new_particle_effect =  UGameplayStatics::SpawnEmitterAttached(
																particle_effect_damage_player,
																Cast<USceneComponent>( GetRootComponent())
															);

				ref_character = Cast<ACharacter>(OtherActor);
				FVector postion_character = ref_character->GetMesh()->GetSocketLocation(TEXT("spine_03_Socket"));

				if (new_particle_effect == nullptr) return;
				new_particle_effect->SetBeamTargetPoint(0, postion_character, 0);
				instance_particle = true;
				//in beginPlay example
				GetWorld()->GetTimerManager().SetTimer(timer_handle_particle, this, &AEnemy::OnTimerOut_particle, timer_to_end_particle, repeat_timer_particle);
				attack_sound->Play();
			}

		}
		
	}
}

//ENEMY DAMAGE si el trigger del enemigo colisiona con algo
void AEnemy::on_component_begin_overlap_damage_enemy(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherComp == nullptr) return;
	if (getting_hit_montage == nullptr) return;//si hay un montage
	if (frozen_sound == nullptr) return;//si hay un montage

	//if (can_receive_hit)
	//{
	if (OtherComp->ComponentHasTag("ice_magic"))//inicio colision con hielo
	{
		if (GetWorld() == nullptr) return;
		attach_ice_effect(ice_effect, "bodySocket");

		AActor* parent_actor = OtherComp->GetOwner();//destruyo la esfera de hielo

		can_attack = false;//el personaje NO puede atacar esta CONGELADO esto lo verifico desde el character
		its_frozen = true;
		GetWorld()->GetTimerManager().SetTimer(timer_handle_ice, this, &AEnemy::OnTimerOut_ice, timer_to_end_ice, repeat_timer_ice);

		//Solo si tiene secuecia cinematica la pongo en pause
		if (secuencia_cinematica != nullptr) secuencia_cinematica->SequencePlayer->Pause();

		frozen_sound->Play();
	}

	if (OtherComp->ComponentHasTag("Sword_collision"))//inicio colision con hielo
	{
		USkeletalMeshComponent* mesh_character = GetMesh();
		if (mesh_character == nullptr) return;//si no hay mesh

		PlayAnimMontage(getting_hit_montage);// ejecuto el montage golpe
		life_Enemy -= 25;//saca vida al enemigo
	}
		
	check_if_destroy_enemy();//siempre verifico si muere el enemigo luego de una colisi�n
	//}
}


//TIMER
void AEnemy::OnTimerOut_visible_mesh()
{
	if (GetWorld() == nullptr) return;
	if (GetMesh() == nullptr) return;

	GetMesh()->SetVisibility(false);
	
	//destroy all attached world outliner child
	TArray<AActor*> object_child_world_outliner{};
	GetAttachedActors(object_child_world_outliner);
	for (AActor* actor_attach : object_child_world_outliner)
	{
		if (actor_attach == nullptr) return;
		actor_attach->Destroy();
	}

	if (game_mode == nullptr) return;

	Cast< AGames_rules_game_Mode>(game_mode)->cant_enemy_dead += 1;
	Cast< AGames_rules_game_Mode>(game_mode)->verify_activate_trigger_win();//ejecuto la funci�n con un delegado

	GetWorld()->GetTimerManager().SetTimer(timer_handle_destroy, this, &AEnemy::OnTimerOut_destroy, timer_to_end_destroy, repeat_timer_destroy);

}

//destruyo el enemigo y tambien puedo hacer alguna otra cosa, 
// como reiniciar el timer por cierto tiempo hasta volver a instanciar, reposicionar o lo que sea
void AEnemy::OnTimerOut_destroy()
{
	Destroy();
}

//TIMER ICE
void AEnemy::OnTimerOut_ice()
{
	//tendria que descongelar al enemigo
	can_attack = true;//el personaje puede atacar ya no esta congelado
	its_frozen = false;
	
	//esto es solo si tengo secuencia cinematico
	if (secuencia_cinematica != nullptr) secuencia_cinematica->SequencePlayer->Play();
	
}


//Attach effect in bone
AActor* AEnemy::attach_ice_effect(TSubclassOf<AActor> effectClass, FName socketName)
{
	if (GetMesh() == nullptr) return nullptr;
	const FTransform orientation_socket = GetMesh()->GetSocketTransform(socketName, ERelativeTransformSpace::RTS_World);

	if (GetWorld() == nullptr) return nullptr;
	AActor* newEffect = GetWorld()->SpawnActor(effectClass, &orientation_socket);//FTransform por referencia constante
	
	if (newEffect == nullptr) return nullptr;
	newEffect->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, socketName);

	return newEffect;
}

//particle effect end
void AEnemy::OnTimerOut_particle()
{
	instance_particle = false;
	new_particle_effect->DestroyComponent();
	new_particle_effect = nullptr;
	ref_character = nullptr;
}


//check if destroy enemy
void AEnemy::check_if_destroy_enemy()
{
	if (life_Enemy <= 0)//paso a la parde del montage donde destruyo el enemigo
	{

	


		can_receive_hit = false;

		if (blueprintAnimation == nullptr) return;

		FName new_Section_fname = TEXT("finalDestroyEnemy");//new section
			//cambio a la siguiente seccion del montage
		blueprintAnimation->Montage_SetNextSection(
			blueprintAnimation->Montage_GetCurrentSection(getting_hit_montage),
			new_Section_fname,
			getting_hit_montage
		);

		//�ltimos pasos antes de destruir el enemigo
		if (GetWorld() == nullptr) return;
		if (Box_Damage_player == nullptr) return;
		if (Box_Damage_Enemy == nullptr) return;
		if (GetCapsuleComponent() == nullptr) return;

		Box_Damage_player->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		Box_Damage_Enemy->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		GetWorld()->GetTimerManager().SetTimer(timer_handle_enemy_damage, this, &AEnemy::OnTimerOut_visible_mesh, timer_to_end_enemy_damage, repeat_timer_enemy_damage); //destruir en 1 segundo

		//set visibility widget
		if (Component_widget == nullptr)
		{
			if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("No se encontro el widget blueprint"));
		}
		if (Component_widget == nullptr) return;
		Component_widget->SetVisibility(false);//widget deja de verse
		
		//Solo si tiene secuecia cinematica la pongo en pause
		if (secuencia_cinematica != nullptr) secuencia_cinematica->SequencePlayer->Pause();
	}
}

