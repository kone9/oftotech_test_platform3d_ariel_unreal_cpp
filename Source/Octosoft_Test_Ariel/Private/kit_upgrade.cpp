// Fill out your copyright notice in the Description page of Project Settings.


#include "kit_upgrade.h"
#include "Components/BoxComponent.h"
#include <Kismet/GameplayStatics.h>
#include <Engine/World.h>

#include "Games_rules_game_Mode.h"
#include "Components/AudioComponent.h"

// Sets default values for this component's properties
Ukit_upgrade::Ukit_upgrade()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...

	game_mode = nullptr;
	effect_to_grab = nullptr;
}


// Called when the game starts
void Ukit_upgrade::BeginPlay()
{
	Super::BeginPlay();

	// ...
	if (GetWorld() == nullptr) return;
	if (GetOwner() == nullptr) return;
	Box = Cast< UBoxComponent>(GetOwner()->GetComponentByClass(UBoxComponent::StaticClass()));
	

	Box->OnComponentBeginOverlap.AddDynamic(this, &Ukit_upgrade::on_component_begin_overlap);
	game_mode = UGameplayStatics::GetGameMode(GetWorld());
}


// Called every frame
void Ukit_upgrade::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void Ukit_upgrade::on_component_begin_overlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	//if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, TEXT("PERSONAJE TIENE SUPER SALTO"));

	if (game_mode == nullptr) return;
	if (OtherActor == nullptr) return;
	if (GetOwner() == nullptr) return;
	
	
	Cast< AGames_rules_game_Mode>(game_mode)->super_double_jump = true;//add score

	const bool my_test_bool = Cast< AGames_rules_game_Mode>(game_mode)->super_double_jump;
	if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, FString::Printf(TEXT("PERSONAJE TIENE SUPER SALTO: %s"), my_test_bool ? TEXT("true") : TEXT("false")));

	//.h
	// If you need to spawn class made it in blueprint you can use TSubClassOf to get the UClass
	//UPROPERTY(EditAnywhere, Category = Spawner)
	//TSubclassOf<AActor> blueprint_class{};

	//.cpp
	// Don't forget to include World.h to use methods inside UWorld Class
	FActorSpawnParameters spawn_params{};
	spawn_params.Owner = nullptr; // Specify owner actor for this new actor if you need one.
	spawn_params.Instigator = nullptr; // Specify instigator pawn for this new actor if you need one
	spawn_params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	//auto* const new_actor_class = Aura_Ice_Shatter->StaticClass(); // If you need other class than AActor you can change with YOURCLASS::StaticClass() to get UClass

	if (GetOwner() == nullptr) return;
	const auto new_actor_transform = GetOwner()->GetTransform(); // Identity Matrix if you need the default transformation.

	// Spawn Actor return a Pointer to new Actor
	AActor* effect = GetWorld()->SpawnActor<AActor>(effect_to_grab, new_actor_transform, spawn_params);
	
	if (effect == nullptr) return;
	effect->AttachToActor(OtherComp->GetAttachmentRootActor(),FAttachmentTransformRules::KeepWorldTransform);
	effect->SetActorTransform(OtherComp->GetAttachmentRootActor()->GetTransform());

	if (kitUpgradeSound == nullptr) return;
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), kitUpgradeSound, GetOwner()->GetActorLocation());

	GetOwner()->Destroy();//destroy this model

}

