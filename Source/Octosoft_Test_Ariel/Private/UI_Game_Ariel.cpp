// Fill out your copyright notice in the Description page of Project Settings.


#include "UI_Game_Ariel.h"
#include <Kismet/GameplayStatics.h>
#include <Engine/World.h>
#include "Components/ProgressBar.h"
#include "Components/TextBlock.h"
#include <Kismet/KismetMathLibrary.h>

#include "Games_rules_game_Mode.h"



void UUI_Game_Ariel::NativeConstruct()
{
	Super::NativeConstruct();
	if (GetWorld() == nullptr) return;

	game_mode = UGameplayStatics::GetGameMode(GetWorld());

}

void UUI_Game_Ariel::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry,InDeltaTime);
	if (game_mode == nullptr) return;
	
	if (Cast< AGames_rules_game_Mode>(game_mode)->super_double_jump == true && counter_no_repeat_double_jump == 0)
	{
		double_Jump->SetText(FText::FromString("Super double jump: Enable"));
		counter_no_repeat_double_jump = 1;
	}

	float life_to_clamp = Cast< AGames_rules_game_Mode>(game_mode)->life / 100;//ejemplo suma
	//const float life_limit = UKismetMathLibrary::FClamp(life_to_clamp, 0, 1.0f);
	
	//const float float_to_print = Cast< AGames_rules_game_Mode>(game_mode)->life / 100;
	//if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("float: %f"), float_to_print));
	//
	lifeBar->SetPercent(life_to_clamp);
}

