// Fill out your copyright notice in the Description page of Project Settings.


#include "Menu_inicio_UserWidget.h"

#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "Ariel_Game_Instance_multinivel.h"
#include "Kismet/GameplayStatics.h"
#include <Engine/World.h>
#include "Engine/GameInstance.h"


void UMenu_inicio_UserWidget::NativeConstruct()
{
	Super::NativeConstruct();
	//if (GetGameInstance() == nullptr) return;
	if (Start_button == nullptr) return;
	if (normal_button == nullptr) return;
	if (hard_button == nullptr) return;

	Start_button->OnClicked.AddDynamic(this, &UMenu_inicio_UserWidget::On_Clicked_Start_button);
	normal_button->OnClicked.AddDynamic(this, &UMenu_inicio_UserWidget::On_Clicked_normal_button);
	hard_button->OnClicked.AddDynamic(this, &UMenu_inicio_UserWidget::On_Clicked_hard_button);
	//
	////ariel_game_instance = Cast<UAriel_Game_Instance_multinivel>(GetGameInstance());
	//my_game_instance = GetGameInstance();
}


void UMenu_inicio_UserWidget::On_Clicked_Start_button()
{
	if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, TEXT("On_Clicked_Start_button C++"));
	Start_button->SetVisibility(ESlateVisibility::Hidden);
	normal_button->SetVisibility(ESlateVisibility::Visible);
	hard_button->SetVisibility(ESlateVisibility::Visible);
}

void UMenu_inicio_UserWidget::On_Clicked_normal_button()
{
	//if (ariel_game_instance == nullptr) return;
	if (GetWorld() == nullptr) return;
	if (GetGameInstance() == nullptr) return;
		
	if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, TEXT("On_Clicked_normal_button C++"));
	
	//ariel_game_instance->dificulty_hard = false;
	Cast<UAriel_Game_Instance_multinivel>(GetGameInstance())->dificulty_hard = false;

	FName nameLevel = TEXT("mapa_principal");
	UGameplayStatics::OpenLevel(GetWorld(), nameLevel);
}

void UMenu_inicio_UserWidget::On_Clicked_hard_button()
{
	//if (ariel_game_instance == nullptr) return;
	if (GetWorld() == nullptr) return;
	if (GetGameInstance() == nullptr) return;


	if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, TEXT("On_Clicked_hard_button C++"));
	
	//if (ariel_game_instance != nullptr) ariel_game_instance->dificulty_hard = true;
	Cast<UAriel_Game_Instance_multinivel>(GetGameInstance())->dificulty_hard = true;
	FName nameLevel = TEXT("mapa_principal");
	UGameplayStatics::OpenLevel(GetWorld(), nameLevel);
}



	

