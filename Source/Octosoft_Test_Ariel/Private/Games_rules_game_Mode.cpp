// Fill out your copyright notice in the Description page of Project Settings.


#include "Games_rules_game_Mode.h"
#include "Blueprint/UserWidget.h"
#include <Engine/World.h>
#include "Ariel_Game_Instance_multinivel.h"

#include <Engine/World.h>

#include <LevelSequenceActor.h>
#include <LevelSequencePlayer.h>
#include <MovieSceneSequencePlayer.h>

#include <Kismet/GameplayStatics.h>

//#include "Sound/AmbientSound.h"
//#include "Components/AudioComponent.h"
//in beginPlay example


///////////


AGames_rules_game_Mode::AGames_rules_game_Mode()
{
	life = 100;
	damage_enemy = 20;
	damage_obstacle = 5;
	cant_enemy_to_end = 9;
	Kit_life = 25;
	timer_to_end_respawn_player =  3.0f;
	is_dead_player = false;
	super_double_jump = false;
	dificulty_hard = false;
	repeat_timer_to_end_respawn_player = false;
}


//LLAMADO DESDE TODOS LOS ENEMIGOS
void AGames_rules_game_Mode::verify_activate_trigger_win()
{
	if (cant_enemy_dead >= cant_enemy_to_end)
	{
		if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, TEXT("tendria que detenerse activar el lugar para terminar el juego"));
	
		if (Sequencer_end_animation == nullptr) return;
		Sequencer_end_animation->SequencePlayer->Play();

		////pause all audios
		/*TArray< AActor*> search_sound;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), AAmbientSound::StaticClass(), search_sound);
		for (AActor* actor_sound : search_sound)
		{
			if (actor_sound == nullptr) return;
			Cast<AAmbientSound>(actor_sound)->GetAudioComponent()->SetPaused(true);
		}*/

		//activo el trigger para terminar el juego con efecto de particulas
		//activo cinematica mostrar donde terminar el juego

	}

}


void AGames_rules_game_Mode::BeginPlay()
{
	Super::BeginPlay();
	if (GetWorld() == nullptr) return;

	/*UAriel_Game_Instance_multinivel* ariel_game_instance =  Cast<UAriel_Game_Instance_multinivel>( GetGameInstance() );
	bool hard = ariel_game_instance->dificulty_hard;*/
	if (Cast<UAriel_Game_Instance_multinivel>(GetGameInstance()) == nullptr) return;
	
	bool hard = Cast<UAriel_Game_Instance_multinivel>(GetGameInstance())->dificulty_hard;//recibo desde el game instance que se mantiene entre niveles, osea se selecciona desde la itnerface

	if (hard == true)//ejecuto dificultad en hard
	{
		damage_enemy = 50;
		damage_obstacle = 20;
	}
	else//ejecuto dificultad en easy
	{
		damage_enemy = 20;
		damage_obstacle = 5;
	}


	TArray< AActor*> Search_end_animation;
	UGameplayStatics::GetAllActorsOfClassWithTag(GetWorld(), ALevelSequenceActor::StaticClass(), TEXT("Activate_end_point_animation"), Search_end_animation);
	Sequencer_end_animation = Cast<ALevelSequenceActor>(Search_end_animation[0]);

	//if (GetWorld() == nullptr) return;
	//if (game_ui == nullptr) return;

	//UUserWidget* new_UI = CreateWidget(GetWorld(), game_ui);
	//
	//if (new_UI == nullptr) return;
	//new_UI->AddToViewport();

}

void AGames_rules_game_Mode::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	//my_EndGameDelegate.Unbind();//cuando esto se destruye desvinculo el delegado
}




