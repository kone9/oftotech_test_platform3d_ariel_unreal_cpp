// Fill out your copyright notice in the Description page of Project Settings.


#include "Destroy_on_time_out.h"
#include <Engine/World.h>

// Sets default values for this component's properties
UDestroy_on_time_out::UDestroy_on_time_out()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UDestroy_on_time_out::BeginPlay()
{
	Super::BeginPlay();

	// ...
	if (GetWorld() == nullptr) return;
	GetWorld()->GetTimerManager().SetTimer(timer_handle, this, &UDestroy_on_time_out::OnTimerOut, timer_to_end_destroy, repeat_timer);
	
	
}


// Called every frame
void UDestroy_on_time_out::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UDestroy_on_time_out::OnTimerOut()
{
	if (GetOwner() == nullptr) return;
	GetOwner()->Destroy();
}

