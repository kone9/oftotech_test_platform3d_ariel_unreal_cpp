// Fill out your copyright notice in the Description page of Project Settings.


#include "End_Game.h"
#include <Engine/World.h>

#include "Components/BoxComponent.h"
#include <LevelSequenceActor.h>
#include <LevelSequencePlayer.h>
#include <MovieSceneSequencePlayer.h>
#include <Kismet/GameplayStatics.h>


// Sets default values for this component's properties
UEnd_Game::UEnd_Game()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;



	// ...
}


// Called when the game starts
void UEnd_Game::BeginPlay()
{
	Super::BeginPlay();


	if (GetOwner() == nullptr) return;
	BoxTrigger_end_game = Cast< UBoxComponent>(GetOwner()->GetComponentByClass(UBoxComponent::StaticClass())); //example of how to find a component using the reference class

	if (BoxTrigger_end_game == nullptr) return;
	BoxTrigger_end_game->OnComponentBeginOverlap.AddDynamic(this, &UEnd_Game::on_component_begin_overlap);
	
}


// Called every frame
void UEnd_Game::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}


void UEnd_Game::on_component_begin_overlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor->ActorHasTag("Jugador"))
	{
		TArray< AActor*> Search_end_animation;
		UGameplayStatics::GetAllActorsOfClassWithTag(GetWorld(), ALevelSequenceActor::StaticClass(), TEXT("Finish_game_sequencer"), Search_end_animation);
		ALevelSequenceActor* Finish_game_sequencer = Cast<ALevelSequenceActor>(Search_end_animation[0]);

		if (Finish_game_sequencer == nullptr) return;
		Finish_game_sequencer->SequencePlayer->Play();

		
	}
	

}

