// Fill out your copyright notice in the Description page of Project Settings.


#include "kit_life/Kit_life_recovery.h"
#include "Components/BoxComponent.h"
#include <Kismet/GameplayStatics.h>
#include <Engine/World.h>

#include "Games_rules_game_Mode.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/AudioComponent.h"

// Sets default values for this component's properties
UKit_life_recovery::UKit_life_recovery()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	//Box = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger_Life"));
	

	game_mode = nullptr;
	Aura_Ice_Shatter = nullptr;
	// ...
}


// Called when the game starts
void UKit_life_recovery::BeginPlay()
{
	Super::BeginPlay();

	if (GetWorld() == nullptr) return;
	if (GetOwner() == nullptr) return;
	

	Trigger_kit_life_Box =  Cast< UBoxComponent>( GetOwner()->GetComponentByClass(UBoxComponent::StaticClass()) );
	game_mode = UGameplayStatics::GetGameMode(GetWorld());
	
	if (Trigger_kit_life_Box == nullptr) return;
	Trigger_kit_life_Box->OnComponentBeginOverlap.AddDynamic(this, &UKit_life_recovery::on_component_begin_overlap);
	
}


// Called every frame
void UKit_life_recovery::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UKit_life_recovery::on_component_begin_overlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

	//if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, TEXT("COLISIONO EL PERSONAJE CON EL KIT DE VIDA"));
	
	if (game_mode == nullptr) return;
	if (GetOwner() == nullptr) return;
	if (Aura_Ice_Shatter == nullptr) return;
	if (Cast< AGames_rules_game_Mode>(game_mode)->life >= 100) return;//If life is equal to 100 I can't get life kit
	if (Cast< AGames_rules_game_Mode>(game_mode)->life <= 1) return;//If life is equal to 100 I can't get life kit
	//if (life_sound == nullptr) return;

	

	//clamp score to 100
	float new_life = Cast< AGames_rules_game_Mode>(game_mode)->life + Cast< AGames_rules_game_Mode>(game_mode)->Kit_life;
	float life_limit = UKismetMathLibrary::FClamp(new_life, 0, 100);
	Cast< AGames_rules_game_Mode>(game_mode)->life = life_limit;//add score

	//.h
	// If you need to spawn class made it in blueprint you can use TSubClassOf to get the UClass
	//UPROPERTY(EditAnywhere, Category = Spawner)
	//TSubclassOf<AActor> blueprint_class{};
	
	//.cpp
	// Don't forget to include World.h to use methods inside UWorld Class
	FActorSpawnParameters spawn_params{};
	spawn_params.Owner = nullptr; // Specify owner actor for this new actor if you need one.
	spawn_params.Instigator = nullptr; // Specify instigator pawn for this new actor if you need one
	spawn_params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	//auto* const new_actor_class = Aura_Ice_Shatter->StaticClass(); // If you need other class than AActor you can change with YOURCLASS::StaticClass() to get UClass

	if (GetOwner() == nullptr) return;
	const auto new_actor_transform = GetOwner()->GetTransform(); // Identity Matrix if you need the default transformation.

	// Spawn Actor return a Pointer to new Actor
	GetWorld()->SpawnActor<AActor>(Aura_Ice_Shatter, new_actor_transform, spawn_params);
	

	if (kitLifeSound == nullptr) return;

	UGameplayStatics::PlaySoundAtLocation(GetWorld(), kitLifeSound, GetOwner()->GetActorLocation());

	GetOwner()->Destroy();//destroy this model
}

