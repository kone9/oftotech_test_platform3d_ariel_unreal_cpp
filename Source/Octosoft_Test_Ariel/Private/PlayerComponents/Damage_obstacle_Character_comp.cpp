// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerComponents/Damage_obstacle_Character_comp.h"
#include "Enemy/Enemy.h"

#include "Components/BoxComponent.h"
#include <Engine/World.h>
#include "Games_rules_game_Mode.h"

#include "GameFramework/Character.h"

#include <LevelSequenceActor.h>
#include <LevelSequencePlayer.h>
#include <MovieSceneSequencePlayer.h>
#include "Kismet/GameplayStatics.h"

#include "Components/AudioComponent.h"
#include "Particles/ParticleSystem.h"

//#include "Components/AudioComponent.h"

//#include "../Octosoft_Test_ArielCharacter.h"

//#include "Life_hadler_Character.h"

//#include "DrawDebugHelpers.h"

// Sets default values for this component's properties
UDamage_obstacle_Character_comp::UDamage_obstacle_Character_comp()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	// ...

	//Box = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxTriggerObstacleComponent"));//// Tira bug por alguna razon, lo busco directo en la jerarquia creado a mano en el blueprint
	//Box->RegisterComponent();
	//GetOwner()->AddInstanceComponent(Box);
	
	//Box->AttachmentCounter(GetOwner()->GetDefaultAttachComponent());
	

	timer_to_end_obstacle_damage = 1.0f;
	repeat_timer_obstacle_damage = true;

	owner = nullptr;

	game_mode = nullptr;
	ref_character = nullptr;

	//life_hadler_Character = nullptr;

}



//in constructor

//in beginPlay
///////////

// Called when the game starts
void UDamage_obstacle_Character_comp::BeginPlay()
{
	Super::BeginPlay();
	
	// ...
	owner = GetOwner();
	if (owner == nullptr) return;
	Box = Cast< UBoxComponent>(GetOwner()->GetComponentByClass(UBoxComponent::StaticClass()));
	//upgrade_sound = Cast< UAudioComponent>( GetOwner()->GetComponentsByTag(UAudioComponent::StaticClass(), "BoxTriggerEvents")[0] );

	if (GetWorld() == nullptr) return;
	game_mode = UGameplayStatics::GetGameMode(GetWorld());

	if (Box == nullptr) return;
	Box->OnComponentBeginOverlap.AddDynamic(this, &UDamage_obstacle_Character_comp::on_component_begin_overlap);
	Box->OnComponentEndOverlap.AddDynamic(this, &UDamage_obstacle_Character_comp::on_component_end_overlap);

	ref_character = UGameplayStatics::GetPlayerCharacter( GetWorld(), 0 );
	//upgrade_sound =  GetOwner()->GetComponentByClass(UAudioComponent::StaticClass()) ;
	//life_hadler_Character = owner->FindComponentByClass<ULife_hadler_Character>();
	//DrawDebugBox(GetWorld(), Box->GetComponentLocation(), Box->GetScaledBoxExtent(), FColor::Purple, true, 999, 0, 5);
	//in BeginPlay
}


// Called every frame
void UDamage_obstacle_Character_comp::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);


}


//events triggers components
void UDamage_obstacle_Character_comp::on_component_begin_overlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherComp == nullptr) return;
	if (check_life_dead() == true) return;//character is dead
	if (OtherComp->ComponentHasTag("Trigger_kit_life")) return;//si colisiono con kit life return
	

	if ( OtherComp->ComponentHasTag("obstacle") )
	{
		//actual life change
		spawn_effect_obstacle_damage();
		check_dead();
		
		GetWorld()->GetTimerManager().SetTimer(timer_handle_obstacle_damage, this, &UDamage_obstacle_Character_comp::OnTimerOut_in_damage_obstacle, timer_to_end_obstacle_damage, repeat_timer_obstacle_damage);
		if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, TEXT("COLISIONO EL PERSONAJE CON UN OBSTACULO!"));
	}

	if (OtherComp->ComponentHasTag("enemy_damage_player"))
	{
		AEnemy* enemy = Cast<AEnemy>(OtherComp->GetOwner());//obtengo el enemigo
		if (enemy == nullptr) return;

		if (enemy->can_attack == true)//solo si el enemigo puede atacar va a golpear al jugador
		{
			//if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Cyan, TEXT("COLISIONASTE CON ENEMIGO"));
			//actual life change
			if (game_mode == nullptr) return;
			float actual_life = Cast< AGames_rules_game_Mode>(game_mode)->life;
			float damage = Cast< AGames_rules_game_Mode>(game_mode)->damage_enemy;
			Cast< AGames_rules_game_Mode>(game_mode)->life = actual_life - damage;
			check_dead();
		}
	}

	
}

//cuando sale del obstaculo deja de recibir da�o
void UDamage_obstacle_Character_comp::on_component_end_overlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherComp == nullptr) return;
	if (check_life_dead() == true) return;//character is dead
	if (owner == nullptr) return;
	if (OtherComp->ComponentHasTag("Trigger_kit_life")) return;//si colisiono con kit life return
	if (OtherComp->ComponentHasTag("Trigger_kit_Upgrade")) return;//si colisiono con kit upgrade return
	
	if (OtherComp->ComponentHasTag("obstacle"))
	{
		owner->GetWorldTimerManager().ClearTimer(timer_handle_obstacle_damage);//stopTimer	
		if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Green, TEXT("va a detener el timer que descuenta vida"));
	}

}

//check if the character is alive or dead
bool UDamage_obstacle_Character_comp::check_life_dead()
{
	if (game_mode == nullptr) return false;
	int actual_life = Cast< AGames_rules_game_Mode>(game_mode)->life;
	if (actual_life < 1)
	{
		Cast< AGames_rules_game_Mode>(game_mode)->is_dead_player = true;
		return true;
	}
	else
	{
		Cast< AGames_rules_game_Mode>(game_mode)->is_dead_player = false;
		return false;
	}
}

//verifica si el jugador esta muerto, si esta muerto ejecuta todo el c�digo de muerte
void UDamage_obstacle_Character_comp::check_dead()
{
	if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, TEXT("ESTIT CHEQUEANDO SI ESTA MUERTO"));
	//verifica si el jugador esta muerto
	if (Cast< AGames_rules_game_Mode>(game_mode)->life < 1)
	{
		if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, TEXT("VIDA ES MENOR A UNO"));
		if (GetWorld() == nullptr) return;
		Cast< AGames_rules_game_Mode>(game_mode)->is_dead_player = true;

		//play dead montage and disable input
															
		if (ref_character == nullptr) 
		{
			if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, TEXT("NO SE ENCONTRE EL PLAYER CONTROLLER"));
			return;
		}
		
		if (dead_montage == nullptr)
		{
			if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, TEXT("NO SE ENCONTRE EL MONTAGE"));
			return;
		}
		ref_character->PlayAnimMontage(dead_montage);
		ref_character->DisableInput(UGameplayStatics::GetPlayerController(GetWorld(), 0));//desable input and get player controller

		GetWorld()->GetTimerManager().SetTimer(timer_handle_destroy_player, this, &UDamage_obstacle_Character_comp::OnTimerOut_destroy_player, Cast< AGames_rules_game_Mode>(game_mode)->timer_to_end_respawn_player, Cast< AGames_rules_game_Mode>(game_mode)->repeat_timer_to_end_respawn_player);

		if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, TEXT("Se ejecuto correctamente el dead, tendria que morir"));
	}
		
}


//efecto cuando toca el fuego y saca vida
void UDamage_obstacle_Character_comp::spawn_effect_obstacle_damage()
{
	if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Red, TEXT("TENDRIA QUE DESCONTAR VIDA"));
	if (check_life_dead() == true) return;//character is dead
	if (GetWorld() == nullptr) return;
	if (game_mode == nullptr) return;

	float actual_life = Cast< AGames_rules_game_Mode>(game_mode)->life;
	float damage = Cast< AGames_rules_game_Mode>(game_mode)->damage_obstacle;
	Cast< AGames_rules_game_Mode>(game_mode)->life = actual_life - damage;

	FTransform transform_character = GetOwner()->GetTransform();
	transform_character.SetLocation(FVector(
		transform_character.GetLocation().X,
		transform_character.GetLocation().Y,
		transform_character.GetLocation().Z - 90));

	//effect fire
	if (effect_fire_particle == nullptr) return;
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), effect_fire_particle, transform_character);

	//fire sound
	if (damage_fire_sound == nullptr) return;
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), damage_fire_sound, transform_character.GetLocation());
}


//timer damage
void UDamage_obstacle_Character_comp::OnTimerOut_in_damage_obstacle()
{
	spawn_effect_obstacle_damage();
	check_dead();

	if (check_life_dead() == true)
	{
		GetOwner()->GetWorldTimerManager().ClearTimer(timer_handle_obstacle_damage);//el parametro es el FTimerHandle
	}
}


//para reiniciar el nivel
void UDamage_obstacle_Character_comp::OnTimerOut_destroy_player()
{
	if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, TEXT("Tendria que reiniciar el nivel"));

	if (GetWorld() == nullptr) return;
	FName nameLevel = TEXT("mapa_principal");
	UGameplayStatics::OpenLevel(GetWorld(), nameLevel);


	////reiniciar persona sin reiniciar nivel, con Bug relacionado a morir el respawn, lo dejo de ejemplo sin terminar.
	//if (game_mode == nullptr) return;
	//game_mode->ResetLevel();

	////con el animation player
	//TArray< AActor*> Search_respawnPlayer_animation;
	//UGameplayStatics::GetAllActorsOfClassWithTag(GetWorld(), ALevelSequenceActor::StaticClass(), TEXT("reload_player_pawn"), Search_respawnPlayer_animation);
	//ALevelSequenceActor* respawnPlayer_game_sequencer = Cast<ALevelSequenceActor>(Search_respawnPlayer_animation[0]);

	//if (respawnPlayer_game_sequencer == nullptr) return;
	//respawnPlayer_game_sequencer->SequencePlayer->Play();

}





