// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Octosoft_Test_Ariel : ModuleRules
{
	public Octosoft_Test_Ariel(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay","LevelSequence", "MovieScene" });
	}
}
