// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Games_rules_game_Mode.generated.h"

/**
 * 
 */



UCLASS()
class OCTOSOFT_TEST_ARIEL_API AGames_rules_game_Mode : public AGameModeBase
{
	GENERATED_BODY()


protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;


public:
	AGames_rules_game_Mode();

public:
	void verify_activate_trigger_win();//verifica si activa el trigger en el lugar correspondiente para terminar el juego


public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float life;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float damage_enemy;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float damage_obstacle;

	UPROPERTY(EditAnywhere)
	float timer_to_end_respawn_player;
	
	UPROPERTY(VisibleAnywhere)
	bool repeat_timer_to_end_respawn_player;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Kit_life;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool dificulty_hard;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool is_dead_player;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool super_double_jump;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	int cant_enemy_dead {0};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int cant_enemy_to_end;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<class UUserWidget> game_ui {};

private:
	class ALevelSequenceActor* Sequencer_end_animation;
//animations
//public:
//	UPROPERTY(EditAnywhere, BlueprintReadOnly)
//	class ALevelSequenceActor* end_point_Cinematic_LevelSequencer{nullptr};
	//TSubclassOf<class ALevelSequenceActor> end_point_Cinematic_LevelSequencer{nullptr};


};
