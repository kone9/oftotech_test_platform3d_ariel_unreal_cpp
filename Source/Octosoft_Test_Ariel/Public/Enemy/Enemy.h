// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Enemy.generated.h"

UCLASS()
class OCTOSOFT_TEST_ARIEL_API AEnemy : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemy();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;



public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class UBoxComponent* Box_Damage_player;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class UBoxComponent* Box_Damage_Enemy;
	
	UFUNCTION()
	void on_component_begin_overlap_damage_Player(class UPrimitiveComponent* OverlappedComp, AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	
	UFUNCTION()
	void on_component_begin_overlap_damage_enemy(class UPrimitiveComponent* OverlappedComp, AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);


	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	float life_Enemy;


	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class UAnimMontage* getting_hit_montage; //load the montage from the editor


	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class UAnimationAsset* damage_animation; //load the montage from the editor

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class ALevelSequenceActor* secuencia_cinematica;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)//cargar el compoent widget 
	class UWidgetComponent* Component_widget;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)//cargar el compoent widget 
	bool instance_particle{false};

	////////////
/// TIMER VISIBLE MESH AND DESACTIVE 
	FTimerHandle timer_handle_enemy_damage;
	UPROPERTY(EditAnywhere)
	float timer_to_end_enemy_damage{ 1.0f };
	UPROPERTY(EditAnywhere)
	bool repeat_timer_enemy_damage{ false };

	UFUNCTION()
	void OnTimerOut_visible_mesh();



/// TIMER DESTROY
	FTimerHandle timer_handle_destroy;
	UPROPERTY(EditAnywhere)
		float timer_to_end_destroy{ 1.0f };
	UPROPERTY(EditAnywhere)
		bool repeat_timer_destroy{ false };

	UFUNCTION()
		void OnTimerOut_destroy();




/// Timer ICE congelado
	FTimerHandle timer_handle_ice;
	UPROPERTY(EditAnywhere)
	float timer_to_end_ice{ 1.0f };
	UPROPERTY(EditAnywhere)
	bool repeat_timer_ice{ false };

	UFUNCTION()
	void OnTimerOut_ice();

	//ICE Effect ATTACH
	UPROPERTY(EditAnywhere)
	TSubclassOf<AActor> ice_effect{};

	AActor* attach_ice_effect(TSubclassOf<AActor> effectClass, FName socketName);


//Timer Particle attack particle
	FTimerHandle timer_handle_particle;
	UPROPERTY(EditAnywhere)
		float timer_to_end_particle{ 1.0f };
	UPROPERTY(EditAnywhere)
		bool repeat_timer_particle{ false };

	UFUNCTION()
		void OnTimerOut_particle();






private:
	UAnimInstance* blueprintAnimation;//get anim instance

	bool can_receive_hit;

	void check_if_destroy_enemy();

	class AGameModeBase* game_mode;

public:
	bool can_attack{ true };
	//bool instance_particle{ false };

	//particles
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class UParticleSystem* particle_effect_damage_player {nullptr};

	class UParticleSystemComponent* new_particle_effect{ nullptr };

	class ACharacter* ref_character {nullptr};


	//particles
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool its_frozen{ false };

	//sound
public:
	//public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UAudioComponent* attack_sound{ nullptr };

	//public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UAudioComponent* frozen_sound{ nullptr };
};
