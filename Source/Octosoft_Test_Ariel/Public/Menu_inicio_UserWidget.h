// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Menu_inicio_UserWidget.generated.h"

/**
 * 
 */
UCLASS()
class OCTOSOFT_TEST_ARIEL_API UMenu_inicio_UserWidget : public UUserWidget
{
	GENERATED_BODY()
	

protected:
	void NativeConstruct() override;//begin play ui


public:
	//buttons
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UButton* Start_button{ nullptr };//create a "Button" with the same as this variable name inside blueprint widget

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UButton* normal_button{ nullptr };//create a "Button" with the same as this variable name inside blueprint widget

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UButton* hard_button{ nullptr };//create a "Button" with the same as this variable name inside blueprint widget

	//texts
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UTextBlock* Start_text{ nullptr };//create a "TextBlock" with the same as this variable name inside blueprint widget

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UTextBlock* normal_text{ nullptr };//create a "TextBlock" with the same as this variable name inside blueprint widget
	
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UTextBlock* hard_text{ nullptr };//create a "TextBlock" with the same as this variable name inside blueprint widget



	UFUNCTION()
	void On_Clicked_Start_button();

	UFUNCTION()
	void On_Clicked_normal_button();

	UFUNCTION()
	void On_Clicked_hard_button();


	//class UAriel_Game_Instance_multinivel* ariel_game_instance { nullptr };
	UGameInstance* my_game_instance { nullptr };






};
