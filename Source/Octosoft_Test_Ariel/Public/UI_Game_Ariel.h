// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UI_Game_Ariel.generated.h"

/**
 * 
 */
UCLASS()
class OCTOSOFT_TEST_ARIEL_API UUI_Game_Ariel : public UUserWidget
{
	GENERATED_BODY()


protected:
	void NativeConstruct() override;
	void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;
	

	///////////////
	/// in .H
public:
	class AGameModeBase* game_mode {nullptr};

	UPROPERTY(meta = (BindWidget))
	class UProgressBar* lifeBar {nullptr };
	
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* double_Jump{ nullptr};

	int counter_no_repeat_double_jump {0};

};
