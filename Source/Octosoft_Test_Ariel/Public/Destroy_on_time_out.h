// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Destroy_on_time_out.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class OCTOSOFT_TEST_ARIEL_API UDestroy_on_time_out : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UDestroy_on_time_out();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


	////////////
/// in .H
	FTimerHandle timer_handle;
	UPROPERTY(EditAnywhere)
	float timer_to_end_destroy{ 7.0f };
	UPROPERTY(EditAnywhere)
	bool repeat_timer{ false };

	UFUNCTION()
	void OnTimerOut();


};
