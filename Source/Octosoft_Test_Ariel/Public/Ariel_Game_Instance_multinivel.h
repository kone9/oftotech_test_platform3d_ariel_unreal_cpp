// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Ariel_Game_Instance_multinivel.generated.h"

/**
 * 
 */
UCLASS()
class OCTOSOFT_TEST_ARIEL_API UAriel_Game_Instance_multinivel : public UGameInstance
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool dificulty_hard { false };
	

};
