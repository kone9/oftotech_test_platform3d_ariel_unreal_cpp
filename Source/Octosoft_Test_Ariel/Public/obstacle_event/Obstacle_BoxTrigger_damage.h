// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "Obstacle_BoxTrigger_damage.generated.h"

/**
 * 
 */
UCLASS()
class OCTOSOFT_TEST_ARIEL_API AObstacle_BoxTrigger_damage : public ATriggerBox
{
	GENERATED_BODY()
	AObstacle_BoxTrigger_damage();


private:
	UPROPERTY(EditAnywhere)
	bool drawDebug;


protected:
	virtual void BeginPlay() override;


public:
	UFUNCTION()
	void ActorBeginOverlap(class AActor* MyOverlappedActor,class AActor* OtherActor);


};
