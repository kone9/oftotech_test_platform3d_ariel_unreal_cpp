// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Damage_obstacle_Character_comp.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class OCTOSOFT_TEST_ARIEL_API UDamage_obstacle_Character_comp : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UDamage_obstacle_Character_comp();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	
//box component collision
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = box_collision, meta = (AllowPrivateAccess = "true"))
	class UBoxComponent* Box;

	AActor* owner;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class ACharacter* ref_character;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class UAnimMontage* dead_montage;


//public:
	//class ULife_hadler_Character* life_hadler_Character;
private:
	class AGameModeBase* game_mode;

protected:
	FTimerHandle timer_handle_obstacle_damage;
	UPROPERTY(EditAnywhere)
	float timer_to_end_obstacle_damage;
	UPROPERTY(EditAnywhere)
	bool repeat_timer_obstacle_damage;


protected:
	FTimerHandle timer_handle_destroy_player;

	UFUNCTION()
	void OnTimerOut_destroy_player();





private:
	bool check_life_dead();

	void check_dead();

	void spawn_effect_obstacle_damage();

public:
	UFUNCTION()
	void OnTimerOut_in_damage_obstacle();


public:
	UFUNCTION()
	void on_component_begin_overlap(class UPrimitiveComponent* OverlappedComp, AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);


	UFUNCTION()
	void on_component_end_overlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

public:
	//public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class USoundBase* damage_fire_sound{ nullptr };

	//public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UParticleSystem* effect_fire_particle{ nullptr };


};
