// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "kit_upgrade.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class OCTOSOFT_TEST_ARIEL_API Ukit_upgrade : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	Ukit_upgrade();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;



private:
	//UPROPERTY(EditAnywhere)
	class UBoxComponent* Box;

	//UPROPERTY(EditAnywhere)
	//AActor* Aura_Ice_Shatter;

	UPROPERTY(EditAnywhere, Category = Spawner)
	TSubclassOf<AActor> effect_to_grab;


private:
	class AGameModeBase* game_mode;



public:
	UFUNCTION()
		void on_component_begin_overlap(class UPrimitiveComponent* OverlappedComp, AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	////In.CPP

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class USoundBase* kitUpgradeSound{ nullptr };

		
};
