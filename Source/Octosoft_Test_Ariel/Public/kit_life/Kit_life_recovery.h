// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Kit_life_recovery.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class OCTOSOFT_TEST_ARIEL_API UKit_life_recovery : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UKit_life_recovery();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


public:
	//UPROPERTY(EditAnywhere)
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class UBoxComponent* Trigger_kit_life_Box {nullptr};

private:
	//UPROPERTY(EditAnywhere)
	//AActor* Aura_Ice_Shatter;

	UPROPERTY(EditAnywhere)
	TSubclassOf<AActor> Aura_Ice_Shatter{ nullptr };



private:
	class AGameModeBase* game_mode;



public:
	UFUNCTION()
	void on_component_begin_overlap(class UPrimitiveComponent* OverlappedComp, AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	////In.CPP


public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class USoundBase* kitLifeSound {nullptr};


		
};



