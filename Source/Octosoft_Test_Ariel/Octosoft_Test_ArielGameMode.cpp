// Copyright Epic Games, Inc. All Rights Reserved.

#include "Octosoft_Test_ArielGameMode.h"
#include "Octosoft_Test_ArielCharacter.h"
#include "UObject/ConstructorHelpers.h"

AOctosoft_Test_ArielGameMode::AOctosoft_Test_ArielGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

}
