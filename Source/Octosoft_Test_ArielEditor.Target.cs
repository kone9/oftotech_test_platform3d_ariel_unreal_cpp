// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class Octosoft_Test_ArielEditorTarget : TargetRules
{
	public Octosoft_Test_ArielEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.Add("Octosoft_Test_Ariel");
	}
}
